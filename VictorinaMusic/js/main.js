window.onload = function () {
	$('#img').hide();	
};
var backEventListener = null;
var music = ["media/Maks.mp3", "media/Hurts - Wonderful Life.mp3",
             "media/This One's For You.mp3", "media/The xx - Performance.mp3"];
var images = ["images/Maks.jpg", "images/hurts.jpg", "images/DavidGuetta.jpg", "images/the_xx.png"];
var rightAnswers = ["Макс Корж", "Hurts", "David Guetta", "The xx"];
var currentIndex = 0;
var currentIndexMax = music.length;
var countRightAnswers = 0;
var pods = false;

var unregister = function() {
    if ( backEventListener !== null ) {
        document.removeEventListener( 'tizenhwkey', backEventListener );
        backEventListener = null;
        window.tizen.application.getCurrentApplication().exit();
    }
}

//Initialize function
var init = function () {
    // register once
    if ( backEventListener !== null ) {
        return;
    }
    
    // TODO:: Do your initialization job
    console.log("init() called");
    
    var backEvent = function(e) {
        if ( e.keyName == "back" ) {
            try {
                if ( $.mobile.urlHistory.activeIndex <= 0 ) {
                    // if first page, terminate app
                    unregister();
                } else {
                    // move previous page
                    $.mobile.urlHistory.activeIndex -= 1;
                    $.mobile.urlHistory.clearForward();
                    window.history.back();
                }
            } catch( ex ) {
                unregister();
            }
        }
    }
    
    
    $('#btn_next').bind("click", function(event, ui){
    	var answer = $('#answer').val();
    	if(answer === rightAnswers[currentIndex]) {
    		countRightAnswers = countRightAnswers + 2;
    		if(pods) {
    			countRightAnswers = countRightAnswers - 1;
    		}
    		
    	}
    	pods = false;
    	currentIndex = currentIndex + 1;
    	if (currentIndex < currentIndexMax) {
    		//switch for next question
    		$('#img').hide();
    		$('#music').attr("src", music[currentIndex]);
    	} else {
    		//Pass parameter for page result 
    		localStorage.score=countRightAnswers;
    		console.log(countRightAnswers);
    		//open page result
    		window.open("result.html");
    	}
    });
    $('#btn_pods').bind("click", function(event, ui){
    	var answer = $('#answer').val();
    	$('#img').show();
    	$('#img').attr("src", images[currentIndex]);
    	pods = true;
    });
    
    $('#check_answer').bind("click", function(event, ui){
    	var answer = $('#answer').val();
    	if(answer === rightAnswers[currentIndex]) {
    		$('#label').text("Правильный ответ");
    	} else {
    		$('#label').text("Неправильный ответ");
    	}
    });
    // add eventListener for tizenhwkey (Back Button)
    document.addEventListener( 'tizenhwkey', backEvent );
    backEventListener = backEvent;
};

$(document).bind( 'pageinit', init );
$(document).unload( unregister );
