var backEventListener = null;
var host = "http://192.168.1.201:3000";

var unregister = function() {
    if ( backEventListener !== null ) {
        document.removeEventListener( 'tizenhwkey', backEventListener );
        backEventListener = null;
        window.tizen.application.getCurrentApplication().exit();
    }
}

//Initialize function
var init = function () {
    // register once
    if ( backEventListener !== null ) {
        return;
    }
    
    // TODO:: Do your initialization job
    console.log("init() called");
    
    var backEvent = function(e) {
        if ( e.keyName == "back" ) {
            try {
                if ( $.mobile.urlHistory.activeIndex <= 0 ) {
                    // if first page, terminate app
                    unregister();
                } else {
                    // move previous page
                    $.mobile.urlHistory.activeIndex -= 1;
                    $.mobile.urlHistory.clearForward();
                    window.history.back();
                }
            } catch( ex ) {
                unregister();
            }
        }
    }
    getPosts();
    
    // add eventListener for tizenhwkey (Back Button)
    document.addEventListener( 'tizenhwkey', backEvent );
    backEventListener = backEvent;
    
    $("#submit").click(function() {
    	var post = {};
    	post.name = $("#name").val();
    	post.filmname = $("#filmname").val();
    	post.point = $("#point").val();
    	sendPost(post);
    })
};

function getPosts() { 	
	$.ajax({
		url: host + "/repos",
		type: "GET",
		dataType: "json",
		success: function(data) {
			showPosts(data);
		},
		error: function(error) {
			alert("error");
			}
	});
}

function showPosts(posts) {
	$("#posts").empty();
	posts.forEach(function(item, i, arr) {
		$("#posts").append('<li>' + item.title + "</li>");
	});
	$("#posts").listview('refresh');
}

function sendPost(post) {
	$.ajax({
		url: host + "/repos",
		type: "POST",
		data: post,
		success: function() {
			document.location = "#page-list";
			getPosts();
		},
		error: function(error) {
			alert("Send error");
		}
	});
}

$(document).bind( 'pageinit', init );
$(document).unload( unregister );